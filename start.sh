#!/bin/bash
/usr/bin/nohup /root/pd/bin/pd -nogui -stderr -open /root/fluctus/puredata/main.pd &
sleep 5
/usr/bin/nohup /usr/bin/python /root/fluctus/distance/distance.py &
exit 0
