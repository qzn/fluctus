# Fluctus HC-SR04

## prerequisites  
### WiringPi  
	git clone https://github.com/BPI-SINOVOIP/BPI-WiringPi.git -b BPI_M1_M1Plus
	cd BPI-WiringPi
	./build
	ldconfig  

## build
	gcc -lwiringPi -O3 -o fluctus-hcsr04 fluctus-hcsr04.c 

## usage
usage: ./fluctus-hcsr04 \<trigger\> \<echo\>  

Where:  
- trigger is the wiringPi trigger pin number. (usually 0)  
- echo is the wiringPi echo pin number. (usually 1)  
