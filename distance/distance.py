#!/usr/bin/python

# Send Ultrasonic sensor data to OSC (Pure Data)

import sys
import OSC
import time
from subprocess import Popen, PIPE

client = OSC.OSCClient()
msg = OSC.OSCMessage()
msg.setAddress("/distance")
msg.append(0)
client.sendto(msg, ('127.0.0.1', 6449))
msg.clearData()

# Get data from hcsr04
while True:
  p = Popen(['/root/fluctus/distance/hcsr04','0','1'],stdout=PIPE, bufsize=1, universal_newlines=True)
  line = p.stdout.readline()
  if line!='Out of range.\n':
    dist = float(line)
    if dist < 184:
      #print(dist)
      freq = dist
      msg.append(freq)
      try:
        client.sendto(msg, ('127.0.0.1', 6449))
        msg.clearData()
      except:
        toto = 0
  time.sleep(0.1)
